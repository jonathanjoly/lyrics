var idGenerator = require("../api/idGenerator")


var Block = require('./Block')


function Lyric(){
    this.id = idGenerator();
    this.name = 'Sans titre';
    this.blocks = [];

    this.indexOfBlock= function(id){
        var index = -1;
        for(var i=0; i< this.blocks.length;i++){
            if(id === this.blocks[i].getId()){
                index = i;
            }
        }
        return index;
    };
}
Lyric.prototype.getName =function(){
    return this.name;
}
Lyric.prototype.getId =function(){
    return this.id;
}
Lyric.prototype.getBlocks =function(){
    return this.blocks;
}
Lyric.prototype.setName =function(name){
    this.name = name;
}
Lyric.prototype.addBlock =function(block){
    this.blocks.push(block);
}
Lyric.prototype.removeBlock =function(id){
    this.blocks.splice(this.indexOfBlock(id),1)
}
Lyric.prototype.createBlock =function(id){
    var block = new Block();
    block.createSentence();
    this.addBlock(block);
}

module.exports = Lyric
