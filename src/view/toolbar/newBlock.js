
var m = require("mithril")

module.exports = {
    view: function(lyricNode) {
        var lyric  = lyricNode.attrs;

        var content = [
            m("span.nav-option",{onclick:function(){
                lyric.createBlock();
            }},
            m('span.nav-option-label',[
                m('span.glyphicon.glyphicon-plus',''),
                m('span',' Ajouter un bloc')
            ])
            )
        ];

        return content;
    }
}
