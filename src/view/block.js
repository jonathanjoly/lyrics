var m = require("mithril")

var SentenceView = require("./sentence")
var App  = require('../models/App')

module.exports = {
    view: function(lyricNode) {
        var lyric = lyricNode.attrs;

        var content = [];

        content = lyric.getBlocks().map(function(block){
            var blockHeader = [
                m('div.block-title',[
                    m("input.block-input-title",{value:block.getName(), type:'text', onkeyup:function(){
                        block.setName(this.value);
                    }}),
                    m('div.block-remove.close', {onclick:function(){
                        lyric.removeBlock(block.getId());
                    }},'X')
                ])
            ];
            var blockContent = [
                m('div.block-content',
                    m(SentenceView,block)
                ),
                m("span.action-link",{onclick:function(){
                  App.lyrics.copyBlock(block);
              }},[
                  m('span.glyphicon.glyphicon-copy',''),
                  m('span'," Copier le bloc")
              ]),
                m("span.action-link",{onclick:function(){
                  block.createSentence();
              }},[
                  m('span.glyphicon.glyphicon-plus',''),
                  m('span'," Ajouter une ligne")
              ]),

            ];



            if(App.lyrics.existSentenceCopy()){
                blockContent.push(
                    m("span.action-link",{onclick:function(){
                      block.addSentence(App.lyrics.getSentenceCopy());
                  }},[
                      m('span.glyphicon.glyphicon-paste',''),
                      m('span'," Coller la phrase")
                  ])
                );
            }




            var blockContent= [blockHeader,blockContent];

            return m('div.block',blockContent);
        });







    /* content = [
        m(".sentences",block.getSentences.map(function(sentence,index){
            sentence.number = index + 1;
            return m(SentenceView,sentence)
        })),

        m("span.action-link",{onclick:function(){
          Files.newSentence(block.attrs.fileId,block.attrs.id);
        }},"+ Ajouter une ligne"),
        m("span.action-link",{onclick:function(){
          Files.copiedBlock = block.attrs;
      }},"Copier le bloque"),
    ];

    if(Files.copiedSentence.text){
        blockContent.push(
            m("span.action-link",{onclick:function(){
                Files.addSentence(block.attrs.fileId,block.attrs.id,Files.copiedSentence);
            }},"Coller la phrase")
        );
    }
      return m("div.block",[
        m('div.block-title',{oncontextmenu:function(){
            console.log("block");
        }},[
            m("input.block-input-title",{value:block.attrs.name, type:'text', onkeyup:function(){
                block.attrs.name = this.value;
            }}),
            m('div.block-remove.close', {onclick:function(){
                Files.removeBlock(block.attrs.fileId,block.attrs.id);
            }},'X')
        ]),*/
        //return m("div.block-content",content)
        return m("div#blocks-container",content);
    }
}
