var m = require("mithril")

module.exports = {
    view: function(lyricsNode) {
        var lyrics  = lyricsNode.attrs;

        var content = [
            m("span.nav-option",{onclick:function(){
                lyrics.create();
            }},
            m('span.nav-option-label',[
                m('span.glyphicon.glyphicon-file',''),
                m('span',' Nouveau Lyric')
            ])
            )
        ];

        return content;
    }
}
