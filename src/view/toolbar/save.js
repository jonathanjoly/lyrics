var m = require("mithril")
var App  = require('../../models/App')

module.exports = {
    view: function() {
        return m("span.nav-option",{onclick:function(){
            var json = App.lyrics.exportJson();

            var dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(json);
            var exportFileDefaultName = App.lyrics.getSelectedLyric().getName()+'.json';
            var linkElement = document.createElement('a');
            linkElement.setAttribute('href', dataUri);
            linkElement.setAttribute('download', exportFileDefaultName);
            linkElement.click();

        }},
        m('span.nav-option-label',[
            m('span.glyphicon.glyphicon-save-file',''),
            m('span',' Télécharger un fichier de sauvegarde')
        ])
        )
    }
}
