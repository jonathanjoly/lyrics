var m = require("mithril")
var Modal = require("../../models/OpenModal")

module.exports = {
    view: function() {

        var content =[
            m('span.nav-option',{onclick:function(){
                Modal.open = true;
            }},
            m('span.nav-option-label',[
                m('span.glyphicon.glyphicon-open-file',''),
                m('span',' Charger un fichier de sauvegarde')
            ])
            )
        ];




        return content;
    }
}
