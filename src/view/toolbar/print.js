var m = require("mithril")


module.exports = {
    view: function() {
        return m("span.nav-option",{onclick:function(){
            window.print();
        }},
        m('span.nav-option-label',[
            m('span.glyphicon.glyphicon-print',''),
            m('span',' Imprimer')
        ])
        )
    }
}
