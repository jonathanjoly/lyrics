var Sentence = require('./Sentence')
var Block = require('./Block')
var Lyric = require('./Lyric')

function Lyrics(){
    this.lyrics = [];
    this.lyric = {};
    this.copy = {
        block : {exist:false,data:{}},
        sentence : {exist:false,data:{}}
    };


    this.indexOfLyric= function(id){
        var index = -1;
        for(var i=0; i< this.lyrics.length;i++){
            if(id === this.lyrics[i].getId()){
                index = i;
            }
        }
        return index;
    };
}
Lyrics.prototype.getLyrics =function(){
    return this.lyrics;
}
Lyrics.prototype.addLyric =function(lyric){
    this.lyrics.push(lyric);
}
Lyrics.prototype.removeLyric =function(id){
    var theSelectedId = this.lyric.getId();

    this.lyrics.splice(this.indexOfLyric(id),1);

    if(id === theSelectedId){
        if(this.hasLyric()){
            this.lyric = this.lyrics[this.lyrics.length-1];
        }
        else{
            this.lyric = {};
        }
    }

}
Lyrics.prototype.hasLyric =function(id){
    return this.lyrics.length > 0;
}
Lyrics.prototype.getSelectedLyric =function(){
    return this.lyric;
}
Lyrics.prototype.setSelectedLyric =function(id){
    this.lyric = this.lyrics[this.indexOfLyric(id)];
}
Lyrics.prototype.create =function(){
    var sentence = new Sentence();
    var block = new Block();
    var lyric = new Lyric();

    block.addSentence(sentence);
    lyric.addBlock(block);
    this.addLyric(lyric);

    this.lyric = lyric;

}
Lyrics.prototype.copyBlock =function(block){
    this.copy.block.exist= true;
    this.copy.block.data = block;
}
Lyrics.prototype.existBlockCopy =function(){
    return this.copy.block.exist;
}
Lyrics.prototype.getBlockCopy =function(){
    var blockToCopy = this.copy.block.data;
    var block = new Block();
    block.setName(blockToCopy.getName());

    var sentences = blockToCopy.getSentences();
    for( var i=0; i < sentences.length;i++){
        var sentence = new Sentence();
        sentence.setText(sentences[i].getText());
        block.addSentence(sentence);
    }
    return block;
}
Lyrics.prototype.copySentence =function(sentence){
    this.copy.sentence.exist= true;
    this.copy.sentence.data = sentence;
}
Lyrics.prototype.existSentenceCopy =function(){
    return this.copy.sentence.exist;
}
Lyrics.prototype.getSentenceCopy =function(){
    var sentence = new Sentence();
    sentence.setText(this.copy.sentence.data.getText());
    return sentence;
}
Lyrics.prototype.exportJson =function(){
    var lyric  = this.lyric;

    var jsonObject = {name:lyric.getName(),blocks:[]};

    var blocks = lyric.getBlocks();
    for(var i=0;i<blocks.length;i++){
        var block = {name:blocks[i].getName(),sentences:[]};
        var sentences = blocks[i].getSentences();
        for(var j=0;j<sentences.length;j++){
            var sentence  = sentences[j].getText();
            block.sentences.push(sentence);
        }
        jsonObject.blocks.push(block);
    }
    return JSON.stringify(jsonObject);
}


Lyrics.prototype.importJson =function(jsonObject){
    //var data = JSON.parse();
    var data = jsonObject;

    var lyric = new Lyric();
    lyric.setName(data.name);

    var blocks = data.blocks;
    for(var i=0;i<blocks.length;i++){
        var block = new Block();
        block.setName(blocks[i].name);

        var sentences = blocks[i].sentences;
        for(var j=0;j<sentences.length;j++){
            var sentence = new Sentence();
            sentence.setText(sentences[j]);
            block.addSentence(sentence);
        }
        lyric.addBlock(block);
    }
    this.addLyric(lyric);
    this.setSelectedLyric(lyric.getId());

}
module.exports = Lyrics
