var Lyrics = require('./Lyrics')

var App= {
    lyrics: {},
    init: function(){
        this.lyrics = new Lyrics();
    }
}

module.exports = App;
