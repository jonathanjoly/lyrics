var m = require('mithril')

var App  = require('../models/App')

var TabsView = require('./tabs')
var ToolbarView = require('./toolbar')
var LyricView = require('./lyric')
var openModalView = require('./modal/openModal')

module.exports = {
    oninit:function(){
        App.init();
    },
    view: function() {
        var lyrics = App.lyrics;

        var headerContent = [
    		m('div#logo-container',
			     m('h1.header-title','Lyrics')
    		),
    		m('div#tabs-container',m(TabsView,App.lyrics))
    	];

        var appContent = [
    		m('header#header', headerContent),
            m('nav#nav',m(ToolbarView,App.lyrics)),
            m('div#content',
				m('div#page',m(LyricView,App.lyrics))
			),
            m(openModalView)
        ];
        return m('div#app',appContent);
    }
}
