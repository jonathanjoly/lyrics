
var m = require("mithril")
var newView = require("./toolbar/new")
var newBlockView = require("./toolbar/newBlock")
var pasteBlockView = require("./toolbar/pasteBlock")
var printView = require("./toolbar/print")
var saveView = require("./toolbar/save")
var openView = require("./toolbar/open")

module.exports = {
    view: function(lyricsNode) {
        var lyrics  = lyricsNode.attrs;
        var content = [];

        if(lyrics.hasLyric()){
            content = [

                m(newView,lyrics),
                m(saveView),
                m(openView),
                m(newBlockView,lyrics.getSelectedLyric()),
                m(pasteBlockView,lyrics.getSelectedLyric()),
                m(printView)
            ];
        }
        else{
            content = [
                m(newView,lyrics),
                m(openView)
            ];
        }



        return m("#nav-options",content);
    }
}
