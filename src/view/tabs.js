var m = require('mithril')


module.exports = {
    view: function(lyricsNode) {
        var lyrics = lyricsNode.attrs;
        var selectedLyric = lyrics.getSelectedLyric();

        var content = [];

        if(lyrics.hasLyric()){
            content = lyrics.getLyrics().map(function(lyric,index) {
                    var item = 'li';

                    if(selectedLyric && lyric){
                        if(selectedLyric.getId() === lyric.getId()){
                          item = 'li.active';
                        }
                    }
                    return m(item,[
                        m('a',{onclick:function(){
                                lyrics.setSelectedLyric(lyric.getId())}
                        },lyric.getName()),
                        m('span.tab-close',{onclick:function(){
                            lyrics.removeLyric(lyric.getId());
                        }},'X')
                    ])
            })
        }

        return m('ul#tabs',content);
    }
}
