var m = require("mithril")

var BlockView = require("./block")

module.exports = {
    view: function(lyricsNode) {
        var lyrics = lyricsNode.attrs;
        var lyric  = lyrics.getSelectedLyric();

        var content = [];

        if(lyrics.hasLyric()){
            content = [
                m('input#page-title-edit',{type:'text',value:lyric.getName(),onkeyup:function(){
                    lyric.setName(this.value);
                }}),
                m(BlockView, lyric)
            ];
        }
        else{
            content=[
                m('h1.home-title','Lyrics'),
                m('p.home-text','Lyrics vous aide dans la création de composition'),
                m('span.action-link',{onclick:function(){
                    lyrics.create();
                }},'Commencer')
            ];
        }

        return m('.file',content);
    }
}
