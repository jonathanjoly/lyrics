var Syllabe = require("../api/Syllabe")
var idGenerator = require("../api/idGenerator")

function Sentence(){
    this.id = idGenerator();
    this.text = '';
}
Sentence.prototype.getText =function(){
    return this.text;
}
Sentence.prototype.getId =function(){
    return this.id;
}
Sentence.prototype.setText =function(text){
    this.text = text;
}
Sentence.prototype.syllabe =function(){
    return Syllabe.count(this.text);
}

module.exports = Sentence;

/*
var Sentence = require('./models/Sentence')


var sentence1 = new Sentence();
var sentence2 = new Sentence();

console.log(sentence1.getId());
console.log(sentence2.getId());


sentence1.setText("toto");
sentence2.setText("tatata");


console.log(sentence1.getText());
console.log(sentence2.getText());


console.log(sentence1.syllabe());
console.log(sentence2.syllabe());
*/
