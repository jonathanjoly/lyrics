var idGenerator = require("../api/idGenerator")
var Sentence = require('./Sentence')

function Block(){
    this.id = idGenerator();
    this.name = 'Nouveau bloc';
    this.sentences = [];

    this.indexOfSentence= function(id){
        var index = -1;
        for(var i=0; i< this.sentences.length;i++){
            if(id == this.sentences[i].getId()){
                index = i;
            }
        }
        return index;
    };
}
Block.prototype.getName =function(){
    return this.name;
}
Block.prototype.getId =function(){
    return this.id;
}
Block.prototype.getSentences =function(){
    return this.sentences;
}
Block.prototype.getSentence =function(id){
    return this.sentences[this.indexOfSentence(id)];
}
Block.prototype.setName =function(name){
    this.name = name;
}
Block.prototype.addSentence =function(sentence){
    this.sentences.push(sentence);
}
Block.prototype.removeSentence =function(id){
    this.sentences.splice(this.indexOfSentence(id),1)
}
Block.prototype.createSentence =function(id){
    var sentence = new Sentence();
    this.addSentence(sentence);
}
Block.prototype.deplaceSentence =function(id,index){

    var sentence = this.getSentence(id);

    if(index === this.sentences.length-1){
        index--;
    }
    if(index >= this.sentences.length){
        index = this.sentences.length-1;
    }

    this.removeSentence(id);
    this.sentences.splice(index,0,sentence);


}
module.exports = Block;
