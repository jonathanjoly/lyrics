var m = require("mithril")


var BlockView = require("./block")

module.exports = {
    view: function(filesNode) {
        var files = filesNode.attrs;
        var file = files.getSelected();

        var content = [];


        if(files.hasFiles()){
            content = [
                m('input',{type:'text',id:'page-title-edit',value:file.getName(),onkeyup:function(){
                    file.setName(this.value);
                }})
            ]
        }
        else{
            content=[
                m('h1','Lyrics'),
                m('p.welcome','Lyrics vous aide dans la création de composition'),
                m('span.action-link',{onclick:function(){
                    files.create();
                }},'Commencer')
            ];
        }


        /*
        if(Files.list.length >0){
            var file = Files.selected;
            content = [
                m('input',{type:'text',id:'page-title-edit',value:file.name,onkeyup:function(){
                    file.name = this.value;
                }}),
                m('div',{id:'blocks-container'},file.blocks.map(function(block){
                  return m(BlockView, block)
                })),
                m("span.action-link",{onclick:function(){
                  Files.newBlock(file.id);
                }},"+ Ajouter un block")
            ];

            if(Files.copiedBlock.name){
                content.push(m("span.action-link",{onclick:function(){
                  Files.addBlock(file.id,Files.copiedBlock);
              }},"Coller le block"));
            }
        }
        else{
            content=[
                m('h1','Lyrics'),
                m('p.welcome','Lyrics vous aide dans la création de composition'),
                m('span.action-link',{onclick:function(){
                    Files.newFile();
                }},'Commencer')
            ];
        }*/
        return m('.file',content);
    }
}
