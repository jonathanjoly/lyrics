var m = require('mithril');
var AppView = require('./view/app')
m.mount(document.body, AppView);



/*
var idGenerator = require("./api/idGenerator")
var Sentence = require('./models/Sentence')
var Block = require('./models/Block')
var Lyric = require('./models/Lyric')
var Lyrics = require('./models/Lyrics')





console.log('TEST SENTENCES ----------------------------------->');
console.log('Create two sentences');
var sentence1 = new Sentence();
var sentence2 = new Sentence();

console.log('Get the sentences id');
console.log('Id sentence 1: '+sentence1.getId());
console.log('Id sentence 2: '+sentence2.getId());

console.log('Set sentences text');
sentence1.setText("sentence1");
sentence2.setText("une sentence2");

console.log('Show sentences text');
console.log('Text sentence 1: '+sentence1.getText());
console.log('Text sentence 2: '+sentence2.getText());

console.log('Count sentences syllabes');
console.log('Syllabes sentence 1: '+sentence1.syllabe());
console.log('Syllabes sentence 2: '+sentence2.syllabe());

console.log('TEST BLOCKS ----------------------------------->');
console.log('Create blocks');
var block1 = new Block();
var block2 = new Block();

console.log('Id of blocks');
console.log('Id block 1: '+block1.getId());
console.log('Id block 2: '+block2.getId());

console.log('Set the name of blocks');
block1.setName("toto");
block2.setName("tatata");

console.log('Get the name of blocks');
console.log('Name block 1: '+block1.getName());
console.log('Name block 2: '+block2.getName());

console.log('Add sentences in blocks');
block1.addSentence(sentence1);
block2.addSentence(sentence1);
block2.addSentence(sentence2);


console.log('Show blocks sentences');
console.log('Show blocks 1 sentences');
console.log(block1.getSentences());
console.log('Show blocks 2 sentences');
console.log(block2.getSentences());

console.log('Remove sentence 1 from block 2');
block2.removeSentence(sentence1.getId());
console.log('Show blocks 2 sentences');
console.log(block2.getSentences());


console.log('TEST LYRIC ----------------------------------->');
console.log('Create lyrics');
var lyric1 = new Lyric();
var lyric2 = new Lyric();

console.log('Id of lyrics');
console.log('Id  lyric 1: '+lyric1.getId());
console.log('Id  lyric 2: '+lyric2.getId());

console.log('Set the name of lyrics');
lyric1.setName("toto");
lyric2.setName("tatata");

console.log('Get the name of lyrics');
console.log('Name  lyric 1: '+lyric1.getName());
console.log('Name  lyric 2: '+lyric2.getName());

console.log('Add block in lyrics');
lyric1.addBlock(block1);
lyric2.addBlock(block1);
lyric2.addBlock(block2);


console.log('Show lyrics blocks');
console.log('Show lyric 1 blocks');
console.log(lyric1.getBlocks());
console.log('Show lyric 2 blocks');
console.log(lyric2.getBlocks());

console.log('Remove block 1 from lyric 2');
lyric2.removeBlock(block1.getId());
console.log('Show lyric 2 blocks');
console.log(lyric2.getBlocks());

console.log('TEST LYRICS ----------------------------------->');
console.log('Create lyrics object');
var lyrics = new Lyrics();



console.log('Add lyric in lyrics');
lyrics.addLyric(lyric1);
lyrics.addLyric(lyric2);

console.log('Show lyrics lyric');
console.log(lyrics.getLyrics());

console.log('Remove lyric 1 from lyrics');
lyrics.removeLyric(lyric1.getId());

console.log('Show lyrics lyric');
console.log(lyrics.getLyrics());

*/
