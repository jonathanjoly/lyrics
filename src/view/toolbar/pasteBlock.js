var App  = require('../../models/App')

var m = require("mithril")

module.exports = {
    view: function(lyricNode) {
        var lyric  = lyricNode.attrs;
        var content = [];

        if(App.lyrics.existBlockCopy()){
            var content = [
                m("span.nav-option",{onclick:function(){
                    lyric.addBlock(App.lyrics.getBlockCopy());
                }},
                m('span.nav-option-label',[
                    m('span.glyphicon.glyphicon-paste',''),
                    m('span',' Coller le bloc')
                ])
                )
            ];
        }


        return content;
    }
}
