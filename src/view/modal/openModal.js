var m = require("mithril")

var Modal = require("../../models/OpenModal")
var App  = require('../../models/App')

module.exports = {
    view: function() {

        var open = Modal.open;
        var content =[];

        if(open === true){
            content=m('div.modal',
                m('div.modal-content',[
                    m('div.modal-header',[
                        m('span.close',{id:'upload-close-crux',onclick:function(){
                                Modal.open = false;
                        }},'X'),
                        m('h2','Charger une lyrics')
                    ]),
                    m('div.modal-body',[
                        m('input[type=file]#upload-input',{value:'Selectioner un fichier de sauvegarde'}),
                        m('div',m('button.accent-button',{id:'upload-button',onclick:function(){

                            var files = document.getElementById('upload-input').files;

                            var update = function(result){
                                console.log(result);
                                App.lyrics.importJson(result);
                                Modal.open = false;
                                var input = document.getElementById('upload-input');

                                if( input !== null){
                                    input.value= '';
                                }

                                document.getElementById('upload-close-crux').click();
                            }

                            if (files.length >= 1) {

                                var fr = new FileReader();
                                fr.onload = function(e) {
                                    update(JSON.parse(e.target.result));
                                }
                                fr.readAsText(files[0]);
                            }
                        }},'Charger'))
                    ])
                ])
            );
        }


        return content;
    }
}
