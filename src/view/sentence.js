var m = require("mithril")

var App  = require('../models/App')

module.exports = {
    view: function(blockNode) {
        var block = blockNode.attrs;
        var content = [];

        content = block.getSentences().map(function(sentence,index){
            var sentenceContent = [
                m('div.sentence-remove.close', {onclick:function(){
                    block.removeSentence(sentence.getId());
                }},'X'),
                m('div.sentence-number', index+1),
                m('input.sentence-input',{value:sentence.getText(),onkeyup:function(){
                  sentence.setText(this.value);
                }}),
                m('div.sentence-res', sentence.syllabe())
            ];

            var sentenceActions = [];

            if(sentence.text){
                sentenceActions.push(m("span.action-link",{onclick:function(){
                    App.lyrics.copySentence(sentence);
                }},[
                    m('span.glyphicon.glyphicon-copy',''),
                    m('span'," Copier la phrase")
                ]))
            }


            var sentenceView =  m('.sentence',[
                m('.sentence-content',{
                    id: sentence.getId(),
                    draggable:true,
                    ondrag:function(event){

                    },
                    ondragstart:function(event){
                        event.dataTransfer.setData("SentenceId", event.target.id);
                    },

                },sentenceContent),
                m('.sentence-actions',sentenceActions)
            ]);

            var dropAreaView = m('.sentence-drop-area',{
                class: 'sentence-drop-area-'+block.getId(),
                id: 'sentence-drop-area-'+sentence.getId(),
                ondrop:function(event){
                    var id = event.dataTransfer.getData("SentenceId");
                    block.deplaceSentence(id,index);
                },
                ondragover:function(event){
                    event.preventDefault();
                }
            },m('span',''));

            var view = [dropAreaView,sentenceView];

            // Si l'élément est le dernier de la liste
            if( index === block.getSentences().length-1){

                var lastArea =  m('.sentence-drop-area',{
                       class: 'sentence-drop-area-'+block.getId(),
                       id: 'sentence-drop-area-last-'+sentence.getId(),
                       ondrop:function(event){
                           var id = event.dataTransfer.getData("SentenceId");
                           block.deplaceSentence(id,block.getSentences().length);
                       },
                       ondragover:function(event){
                           event.preventDefault();
                       }
                   },m('span',''));
                   view = m('',[dropAreaView,sentenceView,lastArea]);
            }

            return view;
        });
        return m('.sentences',content);
    }

}
