var idGenerator = (function(){
    var lastId = 0;

    return function(){
        return lastId+=1;
    }

})();

module.exports = idGenerator
