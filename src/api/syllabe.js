var Syllabe = {
    consonants: ['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z','-','\''],
    count: function(rawText){
        var counter =0;

        if(rawText != undefined){
            var text = rawText.toLowerCase();
            var words = text.split(' ');

            for(var i=0;i<words.length;i++){
                var word = words[i];

                if(word !== ''){
                    counter += this.countSyllabeInWord(word);
                }

            }
        }


        return counter;
    },
    isVowel: function(lettre){
        var vowel = true;
        for(var i=0;i<this.consonants.length;i++){
            if(this.consonants[i] === lettre){
                vowel = false;
            }
        }
        return vowel;
    },
    isConsonant : function(lettre){
        return !this.isVowel(lettre);
    },
    countSyllabeInWord:function(word){
        var counter = 0;

        if(this.isVowel(word[0])){
          counter++;
        }

        if(word !== undefined){
            for(var i=1;i<word.length;i++){
                if(this.isVowel(word[i]) && (this.isConsonant(word[i-1]))){
                  counter++;
                }
            }
            var lastLetter = word[word.length-1];
            var beforeLastLetter =  word[word.length-2];

            if(lastLetter === 'e' && (!this.isVowel(beforeLastLetter))){
                counter--;
            }
            if(0 === counter){
                counter = 1;
            }
        }

        return counter;
    }
}

module.exports = Syllabe
